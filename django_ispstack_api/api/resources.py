from collections import OrderedDict

from django.forms.models import model_to_dict
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_ispstack_access.django_ispstack_access.models import *
from django_ispstack_api_telekom_de_access_check.django_ispstack_api_telekom_de_access_check.api_client import *
from django_mdat_location.django_mdat_location.models import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)


class CpeResource(Resource):
    class Meta:
        queryset = IspAccessCpe.objects.all()
        always_return_data = True
        allowed_methods = ["get"]
        limit = 0
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        cpe_id = str(kwargs["pk"])

        if not cpe_id.isdigit():
            # wrong format
            return

        return IspAccessCpe.objects.get(id=cpe_id)

    def obj_get_list(self, bundle, **kwargs):
        return IspAccessCpe.objects.all()

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = model_to_dict(bundle.obj)
            bundle.data["customer"] = model_to_dict(bundle.obj.customer)

            if bundle.obj.product:
                bundle.data["product"] = model_to_dict(bundle.obj.product)

                if bundle.obj.product.carrier:
                    bundle.data["product"]["carrier"] = model_to_dict(
                        bundle.obj.product.carrier
                    )

                if bundle.obj.product.subproduct:
                    bundle.data["product"]["subproduct"] = model_to_dict(
                        bundle.obj.product.subproduct
                    )

                    if bundle.obj.product.subproduct.product:
                        bundle.data["product"]["subproduct"]["product"] = model_to_dict(
                            bundle.obj.product.subproduct.product
                        )

        return bundle


class MandatoryAttrsResource(Resource):
    class Meta:
        queryset = IspAccessCpe.objects.all()
        always_return_data = True
        allowed_methods = ["get"]
        limit = 0
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def obj_get(self, bundle, **kwargs):
        action_name = str(kwargs["pk"])
        return action_name

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = OrderedDict()
            bundle.data[bundle.obj] = list()

            attribute_data = None

            if bundle.obj == "mdat":
                # Master Data like Radius prefix
                attribute_data = IspAccessMdatAttributes.objects.all()
            elif bundle.obj == "link":
                # Carrier Link
                attribute_data = IspAccessCarrierLinkAttributes.objects.all()
            else:
                # CPE
                attribute_data = IspAccessCpeAttributes.objects.all()

            for attribute in attribute_data:
                row = dict()
                row["attribute"] = attribute.name
                row["settings"] = dict()
                row["settings"]["form_order"] = attribute.form_order
                row["settings"]["form_show"] = attribute.form_show
                row["settings"]["default_value"] = attribute.default_value
                row["settings"]["html_input_type"] = attribute.html_input_type

                bundle.data[bundle.obj].append(row)

        return bundle


class AccessProductAvailabilityResource(Resource):
    class Meta:
        always_return_data = True
        allowed_methods = ["post"]
        limit = 0
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return

    def dehydrate(self, bundle):
        return bundle


class AccessProductAvailabilityAddressResource(AccessProductAvailabilityResource):
    def obj_create(self, bundle, **kwargs):
        city = MdatCities.objects.get(
            country=MdatCountries.objects.get(iso=bundle.data["country_iso"]),
            zip_code=bundle.data["zip_code"],
        )

        street = bundle.data["street"]
        house_nr = str(bundle.data["house_nr"])

        if "house_nr_add" in bundle.data:
            house_nr_add = bundle.data["house_nr_add"]
        else:
            house_nr_add = None

        carrier_products = access_product_check_by_address(
            city=city,
            street=street,
            house_nr=house_nr,
            house_nr_add=house_nr_add,
        )

        available_products = list()

        for carrier_product in carrier_products:
            available_products.append(
                {
                    "code": carrier_product.internal_article.external_id,
                    "name": carrier_product.internal_article.name,
                    "score": carrier_product.product_order,
                }
            )

        bundle.data = {
            "available_products": available_products,
        }

        return bundle


class AccessProductAvailabilityPhoneNumberResource(AccessProductAvailabilityResource):
    def obj_create(self, bundle, **kwargs):
        country_code = bundle.data["country_code"]
        national_code = bundle.data["national_code"]
        subscriber_num = bundle.data["subscriber_num"]

        access_products = access_product_check_by_phone_number(
            country_code=country_code,
            national_code=national_code,
            subscriber_num=subscriber_num,
        )

        return access_products
