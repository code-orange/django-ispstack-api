from django.urls import path, include
from tastypie.api import Api

from django_ispstack_api.django_ispstack_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

v1_api.register(CpeResource())

v1_api.register(MandatoryAttrsResource())

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# AccessProductAvailabilityResource
v1_api.register(AccessProductAvailabilityAddressResource())
v1_api.register(AccessProductAvailabilityPhoneNumberResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
